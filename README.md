Data mining is a mainstay for data in this century while promoting active continual learning. These materials give complete code solutions using concepts of data classification, data wrangling and data extraction.
NumPy, Pandas, SciPy, Scikit-Learn, Keras are utilized to provide the best statistical analysis and predictions.

Data mining. Data analysis. Data prediction. Python. Numpy. Pandas. Keras. Object classification. Object recognition.

